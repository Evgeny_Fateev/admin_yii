
<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
?>

<?php $this->registerCssFile('web/to/style.css'); ?>
<div class="filter-ref-today">
    <!--    <a name="" href=" --><?// yii\helpers\Url::to (['products/viewproduct', 'id'=>$product->id])   ?><!--" >-->
    <?php foreach ($products as $product): ?>
        <div class="section">
            <div class="col-md-3 task-item">
                <div class="thumbnail">
                    <a href=" <?= yii\helpers\Url::to (['/category/masks', 'id' => $product->id])  ?>">
                        <? echo Html::img('@web/'.$product->image, ['class' =>'img-responsive'])?>
                        <div > <?= $product->name ?> </div>
                    </a>
                    <hr>
                    <div> <?= $product->price ?></div>
                    <hr>
                    <div> <?= $product->description ?></div>
                </div>
            </div>
        </div>


    <?php endforeach;?>
</div>

