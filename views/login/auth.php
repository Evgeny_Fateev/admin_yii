
<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
?>
<?/*$form = ActiveForm::begin();*/?><!--
<?/*= $form->field($logins, 'login');*/?>
<?/*= $form->field($logins, 'password');*/?>
<?/*= Html::submitButton('Войти', ['class'=>'btn btn-success']) */?>
--><?php /* ActiveForm::end(); */?>
<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */



$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

<!--    <p>Пожалуйста введите логин и пароль:</p>-->

    <?php $form = ActiveForm::begin()/*[
        'id' => 'login-form',

        'fieldConfig' => [
            'template' => "{label}\n
    <div class=\"col-lg-3\">{input}
        </div>\n
        <div class=\"col-lg-8\">{error}
        </div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); */
    ?>
<div class="container">
    <div class="row">
        <?= $form->field($login_model, 'login',['options' => ['class' => 'col-lg-3']])->textInput(['autofocus' => true]) ?>
    </div>
    <div class="row">
        <?= $form->field($login_model, 'password',['options' => ['class' => 'col-lg-3']])->passwordInput() ?>
    </div>
</div>





    <div class="form-group">
        <div class="col-lg-11">
            <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>


</div>
