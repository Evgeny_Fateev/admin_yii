
<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
?>
<div class="panel panel-default">
    <div class="panel-body">
        <?$form = ActiveForm::begin();?>
        <?= $form->field($register, 'login')->textInput();?>
        <?= $form->field($register, 'password')->passwordInput();?>

        <?= Html::submitButton('Добавить', ['class'=>'btn btn-success']) ?>
        <?php  ActiveForm::end(); ?>
    </div>
</div>
