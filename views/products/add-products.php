<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
?>
<div class="panel panel-default">
    <div class="panel-body">
<?$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);?>
<?php $items = ArrayHelper::map( $category, 'id', 'name')?>
<?= $form->field($products, 'name')->textInput();?>
<?= $form->field($products, 'description')->textInput();?>
<?= $form->field($products, 'price')->textInput();?>
<?= $form->field($products, 'text')->textInput();?>
<?= $form->field($products, 'id_category')->dropDownList($items, ['options'=>['value' => $category->id]]); ?>
<?= $form->field($products, 'image')->fileInput();?>
<?= Html::submitButton('Добавить', ['class'=>'btn btn-success']) ?>
<?php  ActiveForm::end(); ?>
    </div>
</div>