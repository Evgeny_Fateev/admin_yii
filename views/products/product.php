<?php
use yii\helpers\Html;
?>
<div class="section">
    <div class="container">

        <div class="row">
            <h3> <?= $products->name?></h3>
        </div>
        <div class="col-md-3 task-item">
        <div class="thumbnail">
                <? echo Html::img('@web/'.$products->image, ['class' =>'img-responsive'])?>
        </div>
        </div>

                <?= $products->description?>
                <?= $products->price?>
        <div class="row">
            <?= $products->text?>
        </div>



    </div>
</div>