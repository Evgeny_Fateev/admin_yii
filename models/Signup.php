<?php
 namespace app\models;

 use yii\base\Model;

 class Signup extends Model{
     public $login;
     public $password;

     public function rules()
     {
         return [
             [['login', 'password'], 'required'],
             [['login', 'password'], 'string', 'min'=>2, 'max' => 20],
//             ['password', 'validatePassword'],
         ];
     }
     public function signup(){
         $user = new Auth();
         $user->login = $this->login;
         $user->setPassword($this->password);
         $user->save();

     }
 }