<?php
/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 17.07.2017
 * Time: 11:31
 */

namespace app\models;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use Yii;
class Products extends ActiveRecord
{

    public static function tableName()
    {
        return 'product';
    }

    public function attributeLabels()
    {
        return[
            'description' => 'Описание',
            'name' => 'Имя',
            'price' => 'Цена',
            'text' => 'Текст',
            'image' => 'Изображение',
            'id_category' => 'Категория товара',
        ];
    }

    public function rules(){
        return [
            [['image'], 'file', 'extensions' => 'png, jpg'],
            [['description', 'name', 'id_category'], 'required'],
            [['price', 'text'], 'required'],
        ];
    }

    public function upload(){

        if($this->validate()){
           $this->image->name = md5(microtime() . rand(0, 9999));
           $this->image->saveAs("images/{$this->image->name}.{$this->image->extension}");
             return "images/{$this->image->name}.{$this->image->extension}";
        }
        else{
            return false;
        }
    }
    public function getCategory(){
        return $this->hasMany(Category::className(),['id'=>'id_category']);
    }

    function generate_name($number)
    {
        $arr = array('a', 'b', 'c', 'd', 'e', 'f',
            'g', 'h', 'i', 'j', 'k', 'l',
            'm', 'n', 'o', 'p', 'r', 's',
            't', 'u', 'v', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F',
            'G', 'H', 'I', 'J', 'K', 'L',
            'M', 'N', 'O', 'P', 'R', 'S',
            'T', 'U', 'V', 'X', 'Y', 'Z',
            '1', '2', '3', '4', '5', '6',
            '7', '8', '9', '0',);
        $pass = "";
        for ($i = 0; $i < $number; $i++) {
            $index = rand(0, count($arr) - 1);
            $pass .= $arr[$index];
        }
        return $pass;
    }
}