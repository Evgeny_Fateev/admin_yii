<?php


namespace app\models;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use Yii;

class Auth extends ActiveRecord implements IdentityInterface
{


//    public $user = false;

    public static function tableName(){
        return 'auth';
    }
    public function setPassword($password){
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }
    public function validatePassword($password)
    {
        $this->password = Yii::$app->security->validatePassword($password, $this->password);
    }



    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public function getId()
    {
        return $this->id;
    }

    public static function
    findIdentityByAccessToken($token, $type = null)
    {

    }

    public function getAuthKey()
    {

    }

    public function validateAuthKey($authKey)
    {

    }


    public function attributeLabels()
    {
        return[
            'login' => 'Логин',
            'password' => 'Пароль',
        ];
    }

    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser());
        }
    }








}