<?php
/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 18.07.2017
 * Time: 22:37
 */

namespace app\models;

use app\models\Auth;
use yii\base\Model;

class Login extends Model
{
    public $login;
    public $password;

    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            [['login', 'password'], 'string', 'min'=>2, 'max' => 20],
            ['password', 'validatePassword'],
        ];
    }
    public function validatePassword($attribute, $params)
    {
        if(!$this->hasErrors()){
        $user = $this->getUser();
        if (!$user ||  $user->validatePassword($this->password)) {
            $this->addErrors($attribute, 'Логин или пароль не верен');
        }
        }
    }
    public function getUser()
    {
         return Auth::findOne(['login'=>$this->login]);
    }
}