<?php


namespace app\controllers;
use Yii;

use app\models\Category;
use app\models\Products;
use yii\web\Controller;

class CategoryController extends Controller
{
    public function actionAllCategory(){
        $this->view->title = 'Все категории';
        $categories = Category::find()->all();
        $products = Products::find()->with('category')->all();
        $cat  = Category::find()->all();
        return $this->render('all-category', compact('products','categories'));
    }
    public function actionCategory($id)
    {
        $id = Yii::$app->request->get('id');
        $products = Products::find()->where(['id' => $id])->all();
        return $this->render('category', compact('products'));
    }

    public function actionMasks(){
        $this->view->title = "Сварочные маски";
        $products = Products::find()->where(['id_category' => 1])->all();
        return $this->render('masks', compact('products'));
    }
    public function actionGloves(){
        $this->view->title = "Перчатки";
        $products = Products::find()->where(['id_category' => 2])->all();
        return $this->render('masks', compact('products'));
    }

}