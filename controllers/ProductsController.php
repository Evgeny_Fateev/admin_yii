<?php
/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 17.07.2017
 * Time: 12:14
 */

namespace app\controllers;
use app\models\Category;
use app\models\Products;
use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use Imagine\Gd\Image;
use Imagine\Gd;
use Imagine\Image\Box;

use Imagine\Image\BoxInterface;
class ProductsController extends Controller
{
    public function actionAddProducts(){

        $this->view->title = "Добавление продуктов";
        $products = new Products();
        $category = Category::find()->all();

        if ($products->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isPost){
//               $test =  Yii::$app->security->generateRandomString(5);
                $products->image = UploadedFile::getInstance($products, 'image');
                $products->image = $products->upload();
            }
            $products->save();
            $this->redirect('/products/view-products/');
        }
            return $this->render('add-products', compact('products', 'category'));
    }


    public function actionCategory($id){
        $id = Yii::$app->request->get('id');
        $categories = Category::findOne($id);
//        $categories = Category::findOne($id);
        return $this->render('category', compact('categories'));
    }
     public function actionProduct($id){
        $id = Yii::$app->request->get('id');
         $products = Products::find()->with('category')->all();
//        $products = Products::findOne($id);
        return $this->render('product', compact('products'));
     }

}