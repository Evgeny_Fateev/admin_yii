<?php
/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 17.07.2017
 * Time: 11:28
 */

namespace app\controllers;
use app\models\Auth;
use app\models\Signup;
use app\models\Login;
use Yii;
use yii\web\Controller;

class LoginController extends Controller
{
    public function actionRegister(){
//        $register = new Auth();



//
//
//        $register->password = $register->setPassword(1223);
//        if ($register->load(Yii::$app->request->post())){
//            if($register->save()){
//
//                return $this->refresh();
//            }
//            else{
//
//            }
//        }
//        $hash_pass = Yii::$app->getSecurity()->generatePasswordHash($register->password);
        $register = new Signup();
        if (isset($_POST['Signup'])){
            $register->attributes = Yii::$app->request->post('Signup');
            if($register->validate() && $register->signup()){

                return $this->goHome();
            }
        }
        return $this->render('register', compact('register'));
    }
    public function actionAuth(){
        $login_model = new Login();

        if(Yii::$app->request->post('Login')){
            $login_model->attributes = Yii::$app->request->post('Login');
            if ($login_model->validate()){
                Yii::$app->user->login($login_model->getUser());
                return $this->goHome();
            }
        }
        return $this->render('auth',compact('login_model'));
//        if (!Yii::$app->user->isGuest) {
//            return $this->goHome();
////            $this->redirect('products/product');
//        }
//
//        $model = new Auth();
//        if ($model->load(Yii::$app->request->post()) && $model->login()) {
//            $this->redirect('/products/product');
//            return ;
//        }
//        return $this->render('auth', [
//            'model' => $model,
//        ]);
    }

}